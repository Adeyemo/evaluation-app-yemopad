<?php
require_once('config.php');

$getter = file_get_contents('php://input');
$decoded_getter = json_decode($getter, true);
$response = array();

if($decoded_getter['title'] != "" || $decoded_getter['note'] != ""){
	$title = $decoded_getter['title'];
	$note = $decoded_getter['note'];
	$query = "SELECT title FROM notes WHERE title = '$title'";
	$result = $conn->query($query);
	if($result->num_rows > 0){
		$response['status'] = "error";
		$response['message'] = "Title Exists";

		print json_encode($response);
	}
	else{

		$query = "INSERT INTO notes(title, note) VALUES('$title','$note')";
		if($conn->query($query) === TRUE){
			$response['status'] = "success";
			$response['message'] = "Note Added";

			print json_encode($response);
		}

	}
}
else{
	$response['status'] = "error";
	$response['message'] = "Some Field Missing";

	print json_encode($response);
}
?>