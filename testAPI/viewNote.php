<?php
	require_once('config.php');
	$sql = "SELECT * FROM notes";
	$result = $conn->query($sql);
	$response =  array();
	if($result->num_rows > 0){
		$response['status'] = "success";
		$response['message'] = 'All books returned';
		$response['notes'] = array();
		while($note = $result->fetch_assoc()){
			$tmp = array();
			$tmp['_id'] = $note['_id'];
			$tmp['title']= $note['title'];
			$tmp['note']= $note['note'];
		
			array_push($response["notes"],$tmp);
		}
		print json_encode($response);
	}



?>