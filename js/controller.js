Yemopad.controller('setupController', function($scope,$http,$localStorage,$rootScope,$location,$state){
	console.log("Good")
	$scope.showMenu = true;
	if (!$localStorage.user){
		// $location.path('/setup')
	}
	
	
})

Yemopad.controller('noteController', function($scope,$http,$localStorage,$rootScope,$location,$state){
	if(!localStorage.Notes){
		$localStorage.Notes =[]
	}
	if(!localStorage.localAdd){
		$localStorage.localAdd =[]
	}
	if(!localStorage.localDelete){
		$localStorage.localDelete =[]
	}
	$scope.showEdit = false;
	$scope.serviceBase = "http://localhost/Notepad/testAPI/"
	$scope.addNote = function(note){
		var dataset = {
			"title": note.title,
			"note": note.note
		}
		$http.post($scope.serviceBase+'addNote.php', dataset).success(function(response){
			if (response.status == "success"){
				//Added
				console.log(response.message);
				$scope.note = {};
			}
			else{
				console.log(response.message);
			}
		}).error(function(error){
			$localStorage.Notes.push(note)
			$localStorage.localAdd.push(note);
		})

	}

	$scope.viewNote = function(){
		$scope.notes = []
		$http.get($scope.serviceBase+'viewNote.php').success(function(response){
			if(response.status == "success"){
				$scope.notes = response.notes
				$localStorage.Notes = response.notes
			}
		}).error(function(error){
			$scope.notes = $localStorage.Notes
		})
	}
	$scope.viewNote();
	$scope.delete = function(id){
		$http.get($scope.serviceBase+'deleteNote.php?id='+id).success(function(response){
			if(response.status == "success"){
				//give alert of deleted
				$scope.viewNote();
			}
			else{
				//alert response.message
			}
		}).error(function(error){
			//deleting locally
			angular.forEach($localStorage.Notes, function(value,key){
				if(value.id == id){
					$localStorage.Notes.splice($localStorage.Notes.indexOf(value),1);
					$localStorage.localDelete.push(value)
				}
			})
			$scope.viewNote();
			//add to things to be deleted on sync
			// $localStorage.localDelete.push(value)
		})
	}
	$scope.edit = function(note){
		$rootScope.Rnote = note;
		$scope.showEdit = true;
	}
	$scope.cancel = function(note){
		$rootScope.Rnote = {};
		$scope.showEdit = false;
	}
	$scope.save = function(note){
		$rootScope.Rnote= {};
		var dataset = {
			"title": note.title,
			"note": note.note
		}
		$http.post($scope.serviceBase+'editNote.php?id='+note._id, dataset).success(function(response){
			if (response.status == "success"){
				$rootScope.Rnote= {};
				$scope.showEdit = false;
			}
		},function(error){
			//if no network
		})
	}
})


Yemopad.controller('dashController', function($scope,$http,$localStorage,$rootScope,$location,$state, $interval){
	$scope.serviceBase = "http://localhost/Notepad/testAPI/"
	// $interval($scope.sync, $localStorage.synctime)
	$scope.sync = function(){
		//sync locally added
		angular.forEach($localStorage.localAdd, function(value,key){
			$http.post($scope.serviceBase+'addNote.php',value).success(function(response){
				if (response.status == "success"){
					//added
				}
			}).error(function(){
				//if error occurs 
				console.log("Sync Failed")
			})
		})

		//sync deleted
		angular.forEach($localStorage.localDelete, function(value,key){
			$http.get($scope.serviceBase+'/deleteNote/'+value.id).success(function(response){
				if(response.status == "success"){
				//give alert for Deleted
				}
			}).error(function(error){
				//error
				console.log("Sync Failed")
			})
		})
	}
	

})


