var Yemopad = angular.module('Yemopad',['ui.router','ngStorage']);

Yemopad.config(function($stateProvider,$urlRouterProvider){
	$stateProvider

	.state('login',{
		url: '/login',
		templateUrl: 'views/login.html',
		controller: 'testController'
	})
	.state('dash',{
		url: '/dash',
		templateUrl: 'views/dash.html',
		controller: 'dashController'	
	})
	.state('add',{
		url: '/add',
		templateUrl: 'views/add.html',
		controller: 'noteController'
	})
	.state('view',{
		url: '/view',
		templateUrl: 'views/view.html',
		controller: 'noteController'
	})
	.state('settings',{
		url: '/settings',
		templateUrl: 'views/settings.html',
		controller: 'setupController'
	})
	.state('lock',{
		url: '/view',
		templateUrl: 'views/lock.html',
		controller: 'setupController'
	})
	.state('setup', {
		url: '/setup',
		templateUrl: 'views/setup',
		controller: 'setupController'
	})
  $urlRouterProvider.otherwise('/dash');

});